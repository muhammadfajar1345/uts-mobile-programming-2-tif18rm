import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:bayarbayar_app/firstlogin.dart';

class LoginPage extends StatelessWidget {
  String txtUsername;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF00B0FF),
        body: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 50.0,
              ),
              Text(
                "BAYAR-BAYAR",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                ),
              ),
              Text(
                "Bayar Semua Tagihanmu Dimanapun Kapanpun",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 13.0,
                ),
              ),
              SizedBox(
                height: 55.0,
              ),
              SvgPicture.asset(
                'assets/login.svg',
                height: 150.0,
                width: 150.0,
              ),
              SizedBox(
                height: 55.0,
              ),
              TextField(
                style: TextStyle(
                  color: Colors.white,
                ),
                maxLength: 10,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(16),
                  border: OutlineInputBorder(),
                  labelText: 'Username',
                  labelStyle: TextStyle(
                    color: Colors.white,
                  ),
                  prefixIcon: Icon(
                    Icons.account_circle,
                    color: Colors.white,
                  ),
                  helperText: "Enter Your Username",
                  helperStyle: TextStyle(
                    color: Colors.white,
                  ),
                  disabledBorder: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(20.0),
                    ),
                  ),
                ),
                onChanged: (text) {
                  txtUsername = text;
                },
              ),
              SizedBox(
                height: 5.0,
              ),
              TextField(
                style: TextStyle(
                  color: Colors.white,
                ),
                maxLength: 8,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(5),
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                  labelStyle: TextStyle(
                    color: Colors.white,
                  ),
                  prefixIcon: Icon(
                    Icons.vpn_key_rounded,
                    color: Colors.white,
                  ),
                  helperText: "Enter Your Password",
                  helperStyle: TextStyle(
                    color: Colors.white,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(20.0),
                    ),
                  ),
                ),
                obscureText: true,
              ),
              SizedBox(
                height: 10.0,
              ),
              FlatButton(
                minWidth: 240,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.white)),
                child: Text(
                  'LOGIN',
                  style: TextStyle(fontSize: 20.0),
                ),
                color: Colors.white,
                textColor: Color(0xFF00B0FF),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Dashboard(txtUsername: txtUsername),
                    // Test(),
                  ));
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "UTS Version - Semester 5.0",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 10.0,
                ),
              ),
            ],
          ),
        ));
  }
}
