import 'package:flutter/material.dart';

class Topup extends StatefulWidget {
  @override
  _TopupState createState() => _TopupState();
}

class _TopupState extends State<Topup> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 50.0,
          ),
          Text(
            "Masukan Nominal",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
            ),
          ),
          SizedBox(
            height: 55.0,
          ),
          TextField(
            style: TextStyle(
              color: Colors.white,
            ),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(16),
              labelText: 'Rp',
              labelStyle: TextStyle(
                color: Colors.black,
              ),
              helperStyle: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            "Minimal Topup Adlaah Rp 50.000",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontSize: 10.0,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          FlatButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
                side: BorderSide(color: Colors.white)),
            child: Text(
              'SELANJUTNYA',
              style: TextStyle(fontSize: 20.0),
            ),
            color: Color(0xFF00B0FF),
            textColor: Colors.white,
            onPressed: () {},
          ),
        ],
      ),
    ));
  }
}
