import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:qrscan/qrscan.dart' as scanner;
//put this line manually

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: CamScanner(),
    );
  }
}

class CamScanner extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CamScanner();
  }
}

class _CamScanner extends State<CamScanner> {
  String scanresult;

  @override
  void initState() {
    scanresult = "none";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF00B0FF),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "My QR CODE",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            Image.asset(
              'assets/qrcode.png',
              height: 250.0,
              width: 400.0,
            ),
            Text(
              "Value QR : " + scanresult,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            Container(
                margin: EdgeInsets.only(top: 30),
                child: FlatButton(
                    //button to start scanning
                    color: Colors.white,
                    textColor: Color(0xFF00B0FF),
                    colorBrightness: Brightness.dark,
                    onPressed: () async {
                      scanresult = await scanner.scan();
                      setState(() {});
                    },
                    child: Text("Scan QR or Bar Code")))
          ],
        ),
      ),
    );
  }
}
