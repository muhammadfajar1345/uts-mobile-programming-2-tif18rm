import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:bayarbayar_app/topup.dart';

class Maindashboard extends StatefulWidget {
  @override
  _MaindashboardState createState() => _MaindashboardState();
}

class _MaindashboardState extends State<Maindashboard> {
  void showInformation(BuildContext context) {
    Flushbar(
      title: 'Maintenance Information',
      message: 'Transaksi Sedang Dalam Gangguan',
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Colors.blue.shade300,
      ),
      leftBarIndicatorColor: Colors.blue.shade300,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  void showNotification(BuildContext context) {
    Flushbar(
      title: 'Message',
      message: 'Belum Ada Pesan Terbaru',
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Colors.blue.shade300,
      ),
      leftBarIndicatorColor: Colors.blue.shade300,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListView(
          children: <Widget>[
            ColoredBox(
              color: Color(0xFF00B0FF),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    "Saldo: Rp 101.650 ",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.0,
                    ),
                  ),
                  SizedBox(
                    width: 100.0,
                  ),
                  FlatButton.icon(
                    onPressed: () => showNotification(context),
                    icon: Icon(
                      Icons.notifications_active,
                      color: Colors.white,
                    ),
                    label: Text(""),
                  ),
                ],
              ),
            ),
            ColoredBox(
              color: Color(0xFF00B0FF),
              child: Card(
                elevation: 0.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    FlatButton.icon(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => Topup(),
                          ));
                        },
                        icon: Icon(
                          Icons.account_balance_wallet,
                          color: Colors.grey,
                        ),
                        label: Text(
                          "Topup",
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        )),
                    FlatButton.icon(
                        onPressed: null,
                        icon: Icon(Icons.send_to_mobile),
                        label: Text("Transfer")),
                    FlatButton.icon(
                        onPressed: null,
                        icon: Icon(Icons.list),
                        label: Text("Mutasi")),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
              width: 20.0,
              child: const DecoratedBox(
                decoration: const BoxDecoration(
                  color: Color(0xFF00B0FF),
                ),
              ),
            ),
            ColoredBox(
              color: Colors.white,
              child: Card(
                elevation: 0.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.electrical_services,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "PLN",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.phone_android,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "PULSA",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.water_damage_outlined,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "PDAM",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ColoredBox(
              color: Colors.white,
              child: Card(
                elevation: 0.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.phone,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "TELKOM",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.medical_services,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "BPJS",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.wifi,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "INTERNET",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ColoredBox(
              color: Colors.white,
              child: Card(
                elevation: 0.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.play_arrow,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "Voucher",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.computer,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "Television",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      elevation: 10,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            FlatButton.icon(
                              onPressed: () => showInformation(context),
                              icon: Icon(
                                Icons.wifi_protected_setup,
                                color: Colors.grey,
                                size: 50,
                              ),
                              label: Text(""),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "RERTRIBUSI",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Card(
                elevation: 0.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Image.asset(
                      'assets/ads.png',
                      height: 250.0,
                      width: 400.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
