import 'package:flutter/material.dart';
import 'dart:async';
import 'package:bayarbayar_app/menucontrol.dart';

class Dashboard extends StatefulWidget {
  String txtUsername;
  Dashboard({Key key, @required this.txtUsername}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState(txtUsername);
}

class _DashboardState extends State<Dashboard> {
  void initState() {
    super.initState();
    splashscreenStart();
  }

  splashscreenStart() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration, () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Menucontrol()),
      );
    });
  }

  String txtUsername;
  _DashboardState(this.txtUsername);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF00B0FF),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Selamat Datang Kembali ",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            Text(
              "`" + txtUsername + "`",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
            SizedBox(
              height: 55.0,
            ),
            Image.asset(
              'assets/profile.jpg',
              height: 150.0,
              width: 150.0,
            ),
            SizedBox(
              height: 55.0,
            ),
            // FlatButton(
            //   minWidth: 100,
            //   shape: RoundedRectangleBorder(
            //       borderRadius: BorderRadius.circular(20.0),
            //       side: BorderSide(color: Color(0xFF00B0FF))),
            //   child: Text(
            //     'Go To Dashboard',
            //     style: TextStyle(fontSize: 20.0),
            //   ),
            //   color: Colors.white,
            //   textColor: Color(0xFF00B0FF),
            //   onPressed: () {
            //     // Navigator.of(context).push(MaterialPageRoute(
            //     //   builder: (context) => MyStatefulWidget(),
            //     // ));
            //   },
            // )
            Text(
              "Bayar Tagihan Dimanapun & Kapanpun",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 17.0,
              ),
            ),
            Text(
              "Menjadi Lebih Mudah",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 17.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
