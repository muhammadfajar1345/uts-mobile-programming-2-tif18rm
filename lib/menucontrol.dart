import 'package:flutter/material.dart';
import 'package:bayarbayar_app/maindashboard.dart';
import 'package:bayarbayar_app/scan.dart';
import 'package:bayarbayar_app/histori.dart';

class Menucontrol extends StatefulWidget {
  @override
  _MenucontrolState createState() => _MenucontrolState();
}

class _MenucontrolState extends State<Menucontrol> {
  int _selectedIndex = 0;
  final List<Widget> _widgetOptions = [
    Maindashboard(),
    MyApp(),
    History(),
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.qr_code),
            label: 'QR COde',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list_alt),
            label: 'History',
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.info),
          //   label: 'About',
          // ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.logout),
          //   label: 'About',
          // ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xFF00B0FF),
        onTap: _onItemTapped,
      ),
    );
  }
}
