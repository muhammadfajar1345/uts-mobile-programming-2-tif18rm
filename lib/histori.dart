import 'package:flutter/material.dart';

void main() => runApp(History());

class History extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'Basic List';

    return MaterialApp(
      title: title,
      home: Scaffold(
        body: ListView(
          children: <Widget>[
            ListTile(
              title: Text(
                "PLN POSTPAID",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
              subtitle: Text(
                "99100000012 \nSuccess",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: DecoratedBox(
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(20.0)),
                child: const Text(' - Rp 123.500 '),
              ),
              isThreeLine: true,
            ),
            ListTile(
              title: Text(
                "PLN PREPAID",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
              subtitle: Text(
                "1423456789 \nSuccess",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: DecoratedBox(
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(20.0)),
                child: const Text(' - Rp 23.000 '),
              ),
              isThreeLine: true,
            ),
            ListTile(
              title: Text(
                "PLN PREPAID",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
              subtitle: Text(
                "1423456789 \nSuccess",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: DecoratedBox(
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(20.0)),
                child: const Text(' - Rp 23.000 '),
              ),
              isThreeLine: true,
            ),
            ListTile(
              title: Text(
                "TRANSFER SALDO",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
              subtitle: Text(
                "081342420281 \nSuccess",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: DecoratedBox(
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(20.0)),
                child: const Text(' - Rp 100.000 '),
              ),
              isThreeLine: true,
            ),
            ListTile(
              title: Text(
                "PLN PREPAID",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
              subtitle: Text(
                "1423456789 \nSuccess",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: DecoratedBox(
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(20.0)),
                child: const Text(' - Rp 23.000 '),
              ),
              isThreeLine: true,
            ),
            // ListTile(
            //   title: Text(
            //     "TAMBAH SALDO",
            //     style: TextStyle(
            //       fontWeight: FontWeight.bold,
            //       fontSize: 15.0,
            //     ),
            //   ),
            //   subtitle: Text(
            //     "Bank BCA \nSuccess",
            //     style: TextStyle(
            //       fontWeight: FontWeight.bold,
            //     ),
            //   ),
            //   trailing: DecoratedBox(
            //     decoration: BoxDecoration(
            //         color: Colors.green,
            //         borderRadius: BorderRadius.circular(20.0)),
            //     child: const Text(' - Rp 250.00 '),
            //   ),
            //   isThreeLine: true,
            // ),
            ListTile(
              title: Text(
                "PDAM KOTA BANDUNG",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
              subtitle: Text(
                "AA01230232 \nSuccess",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: DecoratedBox(
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(20.0)),
                child: const Text(' - Rp 87.000 '),
              ),
              isThreeLine: true,
            ),
            ListTile(
              title: Text(
                "VOUCHER",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
              subtitle: Text(
                "GPLAY #123098123 \nSuccess",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: DecoratedBox(
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(20.0)),
                child: const Text(' - Rp 120.000 '),
              ),
              isThreeLine: true,
            ),
          ],
        ),
      ),
    );
  }
}
