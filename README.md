## APLIKASI BAYAR BAYAR 
![ic_launcher](/uploads/f3248aa31b8e583e4dd6098d8b6bb6ad/ic_launcher.png)



## Our Teams
**UTS MATAKULIAH MOBILE PROGRAMMING 2**
**-**
**TIF 18 CNS REGULER MALAM**
* [Muhammad Fajar Wibisono - 16111220](https://www.linkedin.com/in/fajarwibisono/)
* [Oktaviana Widyasari - 18111123]
* [Arnika Marbun - 18111289]
* [Arni Marince Leo - 18111185]


## JOBDESC
### MUHAMMAD FAJAR WIBISONO - 1611122
* [Lead The Teams]
* [Tells The Concept]
* [Write Down The Code]
* [Build The Application]
* [Test Application]
* [Upload Apps To GIT]


### OKTAVIANA WIDYASARI - 18111123
* [Discuss About The UI & Menu]
* [Doing UAT / User Acceptence Test]

### ARNIKA MARBUN - 18111289
* [Discuss About The UI & Menu]
* [Doing UAT / User Acceptence Test]

### ARNI MARINCE LEO - 18111185
* [Discuss About The UI & Menu]
* [Doing UAT / User Acceptence Test]


## SCREENCAPTURE & DOCUMENTATION
### Splashscreen & Login Page
![SmartSelect_20201206-191741](/uploads/d525bb2111f4a3fdfeead9f999aadd14/SmartSelect_20201206-191741.gif)

### Login Process
![SmartSelect_20201206-193905_Video_Player](/uploads/46c2f5b978c6ce4e11c27b465caa314d/SmartSelect_20201206-193905_Video_Player.gif)

### Dashboard, QR Code And History Menu
![SmartSelect_20201206-194016_Video_Player](/uploads/5a107d60d9ef1fa143f4971d5571b809/SmartSelect_20201206-194016_Video_Player.gif)
![SmartSelect_20201206-194058_Video_Player](/uploads/ff90ac1d6e3cfd49adbc25ddb0549bf5/SmartSelect_20201206-194058_Video_Player.gif)



[Flutter logo]: https://raw.githubusercontent.com/flutter/website/master/src/_assets/image/flutter-lockup.png
[flutter.dev]: https://flutter.dev
